
public class Libreria {
    private String cadena;
    private double resultado;
    private boolean suma;
    private boolean resta;
    private boolean multiplicacion;
    private boolean division;
    private boolean potencia;
    private boolean raiz;
    private boolean seno;
    private String ac;
    
    public Libreria(){
        cadena="";
        suma=false;
        resta=false;
        multiplicacion=false;
        division=false;
        potencia=false;
        raiz=false;
        seno=false;
        ac="";
    }
    
    public String concatenamiento(String cadena){
        this.cadena=this.cadena+cadena;
        return this.cadena;
    }
    
    public void suma (String cadena){
        this.resultado=Double.parseDouble(cadena);
        suma=true;
        this.cadena="";
    }
    
    public void resta (String cadena){
        this.resultado=Double.parseDouble(cadena);
        resta=true;
        this.cadena="";
    }
    
    public void multipliacion (String cadena){
        this.resultado=Double.parseDouble(cadena);
        multiplicacion=true;
        this.cadena="";
    }
    
    public void division (String cadena){
        this.resultado=Double.parseDouble(cadena);
        division=true;
        this.cadena="";
    }
    
    public void raiz (String cadena){
        this.resultado=Double.parseDouble(cadena);
        raiz=true;
        this.cadena="";
    }
    
    public void potencia (String cadena){
        this.resultado=Double.parseDouble(cadena);
        potencia=true;
        this.cadena="";
    }
    
    public void seno (String cadena){
        this.resultado=Double.parseDouble(cadena);
        seno=true;
        this.cadena="";
    }
    
    
    public double resultado(String numero){
        if(suma==true){
            resultado=resultado+Double.parseDouble(numero);
        }
        if(resta==true){
            resultado=resultado-Double.parseDouble(numero);
        }
        if(multiplicacion==true){
            resultado=resultado*Double.parseDouble(numero);
        }
        if(division==true){
            resultado=resultado/Double.parseDouble(numero);
        }
        if(raiz==true){
            resultado=Math.sqrt(resultado);
        }
        if(potencia==true){
            resultado=Math.pow(resultado, 2);
        }
        if(seno==true){
            resultado=Math.sin(resultado);
        }
        
        suma=false;
        resta=false;
        multiplicacion=false;
        division=false;
        raiz=false;
        potencia=false;
        seno=false;
        return resultado;        
    }

    public void borrar(){           
        this.cadena="";
    }
}
